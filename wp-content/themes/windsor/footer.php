<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 * 
 */
?>

	</div>
	</div><!-- #main -->
	
	<footer id="colophon" role="contentinfo">
		<div class="container_12">
			<div class="grid_3">
				<p><img src="<?php echo get_bloginfo('url'); ?>/images/footer/windsor_logo.png" height="46" width="140" title="Windsor Caravans - Travelling with you" alt="Windsor Caravans - Travelling with you" border="0" /></p>
				<p><strong>Windsor Caravans</strong></p>
				<p>Windsor Caravans Pty Ltd<br />Phone: (+618) 9352 0900</p>
				<p><img src="<?php echo get_bloginfo('url'); ?>/images/fleetwood_logo.png" width="140" height="35" alt="Fleetwood Corporation" title="Fleetwood Corporation" border="0" /></p>
			</div>
			<div class="grid_3">
				<strong>About Windsor</strong>
				<ul>
					<li><a href="<?php echo get_bloginfo('url'); ?>/why-windsor/">Why Windsor</a></li>
					<li><a href="<?php echo get_bloginfo('url'); ?>/warranty-roadside-assistance/">Windsor Warranty</a></li>
					<li><a href="<?php echo get_bloginfo('url'); ?>/warranty-roadside-assistance/">Roadside Assistance</a></li>
					<li><a href="<?php echo get_bloginfo('url'); ?>/servicing/">Servicing</a></li>
					<li><a href="<?php echo get_bloginfo('url'); ?>/contact/">Contact</a></li>
				</ul>
			</div>
			<div class="grid_3">
				<strong><a href="<?php echo get_bloginfo('url'); ?>/our-range/">Our Range</a></strong>
				<ul>
					<li><a href="<?php echo get_bloginfo('url'); ?>/our-range/rapid/">Rapid</a></li>
					<!-- <li><a href="<?php echo get_bloginfo('url'); ?>/our-range/entice/">Entice</a></li>-->
					<li><a href="<?php echo get_bloginfo('url'); ?>/our-range/genesis/">Genesis</a></li>
					<li><a href="<?php echo get_bloginfo('url'); ?>/our-range/royale/">Royale</a></li>
				</ul>
			</div>
			<div class="grid_3">
				<strong><a href="<?php echo get_bloginfo('url'); ?>/our-dealers">Our Dealers</a></strong>
				<ul>
					<li><a href="<?php echo get_bloginfo('url'); ?>/our-dealers/#new-south-wales">New South Wales</a></li>
					<li><a href="<?php echo get_bloginfo('url'); ?>/our-dealers/#victoria">Victoria</a></li>
					<li><a href="<?php echo get_bloginfo('url'); ?>/our-dealers/#queensland">Queensland</a></li>
					<li><a href="<?php echo get_bloginfo('url'); ?>/our-dealers/#western-australia">Western Australia</a></li>
					<li><a href="<?php echo get_bloginfo('url'); ?>/our-dealers/#south-australia">South Australia</a></li>
					<li><a href="<?php echo get_bloginfo('url'); ?>/our-dealers/#northern-territory">Northern Territory</a></li>
				</ul>
			</div>
		</div>
	</footer><!-- #colophon -->
	</div>
</div><!-- #page -->

<?php wp_footer(); ?>

<script type='text/javascript'>
	jQuery(document).ready(function() {
		jQuery("#submenu a, .page-id-8 a").each(function() {
			jQuery(this).click(function(e) {
				goToByScroll(jQuery(this).attr("href"),500,20);
				return false;
			})
		});
		
		jQuery(".btt").each(function() {
			jQuery(this).click(function(e) {
				goToByScroll("#top",500,0);
				return false;
			});
		});
		
		jQuery('#range-btn-royale').hover(function() {
            jQuery('img', this).stop().animate({
                height: 102 + 10,
                width: 220 + 22,
                marginLeft: -11,
                marginTop: -5
            }, 100);
        }, function() {
            jQuery('img', this).stop().animate({
                    height: 102,
                    width: 220,
                    marginLeft: 0,
                    marginTop: 0
                }, 100);
        });
            
	    jQuery('#range-btn-entice').hover(function() {
            jQuery('img', this).stop().animate({
                height: 102 + 10,
                width: 220 + 22,
                marginLeft: -11,
                marginTop: -5
            }, 100);
        }, function() {
            jQuery('img', this).stop().animate({
                    height: 102,
                    width: 220,
                    marginLeft: 0,
                    marginTop: 0
                }, 100);
        });
	    jQuery('#range-btn-rapid').hover(function() {
            jQuery('img', this).stop().animate({
                height: 102 + 10,
                width: 220 + 22,
                marginLeft: -11,
                marginTop: -5
            }, 100);
        }, function() {
            jQuery('img', this).stop().animate({
                    height: 102,
                    width: 220,
                    marginLeft: 0,
                    marginTop: 0
                }, 100);
        });
	});

	function goToByScroll(id,speed,extra) {
		if(!extra) {extra=0};
		if(jQuery(window).scrollTop() != (parseInt(jQuery(id).offset().top)-extra)) {
			jQuery('html,body').animate({scrollTop: jQuery(id).offset().top - extra}, speed, 'easeOutQuad');
		}
	}

	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-35132369-1']);
	_gaq.push(['_trackPageview']);

	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
  
	jQuery.easing["jswing"]=jQuery.easing["swing"];jQuery.extend(jQuery.easing,{def:"easeOutQuad",swing:function(a,b,c,d,e){return jQuery.easing[jQuery.easing.def](a,b,c,d,e)},easeInQuad:function(a,b,c,d,e){return d*(b/=e)*b+c},easeOutQuad:function(a,b,c,d,e){return-d*(b/=e)*(b-2)+c},easeInOutQuad:function(a,b,c,d,e){if((b/=e/2)<1)return d/2*b*b+c;return-d/2*(--b*(b-2)-1)+c},easeInCubic:function(a,b,c,d,e){return d*(b/=e)*b*b+c},easeOutCubic:function(a,b,c,d,e){return d*((b=b/e-1)*b*b+1)+c},easeInOutCubic:function(a,b,c,d,e){if((b/=e/2)<1)return d/2*b*b*b+c;return d/2*((b-=2)*b*b+2)+c},easeInQuart:function(a,b,c,d,e){return d*(b/=e)*b*b*b+c},easeOutQuart:function(a,b,c,d,e){return-d*((b=b/e-1)*b*b*b-1)+c},easeInOutQuart:function(a,b,c,d,e){if((b/=e/2)<1)return d/2*b*b*b*b+c;return-d/2*((b-=2)*b*b*b-2)+c},easeInQuint:function(a,b,c,d,e){return d*(b/=e)*b*b*b*b+c},easeOutQuint:function(a,b,c,d,e){return d*((b=b/e-1)*b*b*b*b+1)+c},easeInOutQuint:function(a,b,c,d,e){if((b/=e/2)<1)return d/2*b*b*b*b*b+c;return d/2*((b-=2)*b*b*b*b+2)+c},easeInSine:function(a,b,c,d,e){return-d*Math.cos(b/e*(Math.PI/2))+d+c},easeOutSine:function(a,b,c,d,e){return d*Math.sin(b/e*(Math.PI/2))+c},easeInOutSine:function(a,b,c,d,e){return-d/2*(Math.cos(Math.PI*b/e)-1)+c},easeInExpo:function(a,b,c,d,e){return b==0?c:d*Math.pow(2,10*(b/e-1))+c},easeOutExpo:function(a,b,c,d,e){return b==e?c+d:d*(-Math.pow(2,-10*b/e)+1)+c},easeInOutExpo:function(a,b,c,d,e){if(b==0)return c;if(b==e)return c+d;if((b/=e/2)<1)return d/2*Math.pow(2,10*(b-1))+c;return d/2*(-Math.pow(2,-10*--b)+2)+c},easeInCirc:function(a,b,c,d,e){return-d*(Math.sqrt(1-(b/=e)*b)-1)+c},easeOutCirc:function(a,b,c,d,e){return d*Math.sqrt(1-(b=b/e-1)*b)+c},easeInOutCirc:function(a,b,c,d,e){if((b/=e/2)<1)return-d/2*(Math.sqrt(1-b*b)-1)+c;return d/2*(Math.sqrt(1-(b-=2)*b)+1)+c},easeInElastic:function(a,b,c,d,e){var f=1.70158;var g=0;var h=d;if(b==0)return c;if((b/=e)==1)return c+d;if(!g)g=e*.3;if(h<Math.abs(d)){h=d;var f=g/4}else var f=g/(2*Math.PI)*Math.asin(d/h);return-(h*Math.pow(2,10*(b-=1))*Math.sin((b*e-f)*2*Math.PI/g))+c},easeOutElastic:function(a,b,c,d,e){var f=1.70158;var g=0;var h=d;if(b==0)return c;if((b/=e)==1)return c+d;if(!g)g=e*.3;if(h<Math.abs(d)){h=d;var f=g/4}else var f=g/(2*Math.PI)*Math.asin(d/h);return h*Math.pow(2,-10*b)*Math.sin((b*e-f)*2*Math.PI/g)+d+c},easeInOutElastic:function(a,b,c,d,e){var f=1.70158;var g=0;var h=d;if(b==0)return c;if((b/=e/2)==2)return c+d;if(!g)g=e*.3*1.5;if(h<Math.abs(d)){h=d;var f=g/4}else var f=g/(2*Math.PI)*Math.asin(d/h);if(b<1)return-.5*h*Math.pow(2,10*(b-=1))*Math.sin((b*e-f)*2*Math.PI/g)+c;return h*Math.pow(2,-10*(b-=1))*Math.sin((b*e-f)*2*Math.PI/g)*.5+d+c},easeInBack:function(a,b,c,d,e,f){if(f==undefined)f=1.70158;return d*(b/=e)*b*((f+1)*b-f)+c},easeOutBack:function(a,b,c,d,e,f){if(f==undefined)f=1.70158;return d*((b=b/e-1)*b*((f+1)*b+f)+1)+c},easeInOutBack:function(a,b,c,d,e,f){if(f==undefined)f=1.70158;if((b/=e/2)<1)return d/2*b*b*(((f*=1.525)+1)*b-f)+c;return d/2*((b-=2)*b*(((f*=1.525)+1)*b+f)+2)+c},easeInBounce:function(a,b,c,d,e){return d-jQuery.easing.easeOutBounce(a,e-b,0,d,e)+c},easeOutBounce:function(a,b,c,d,e){if((b/=e)<1/2.75){return d*7.5625*b*b+c}else if(b<2/2.75){return d*(7.5625*(b-=1.5/2.75)*b+.75)+c}else if(b<2.5/2.75){return d*(7.5625*(b-=2.25/2.75)*b+.9375)+c}else{return d*(7.5625*(b-=2.625/2.75)*b+.984375)+c}},easeInOutBounce:function(a,b,c,d,e){if(b<e/2)return jQuery.easing.easeInBounce(a,b*2,0,d,e)*.5+c;return jQuery.easing.easeOutBounce(a,b*2-e,0,d,e)*.5+d*.5+c}})

</script>
</body>
</html>