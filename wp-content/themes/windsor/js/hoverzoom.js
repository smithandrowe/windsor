jQuery.fn.extend({ 
     hoverZoom: function(settings) {
 
            var defaults = {
                overlay: true,
                overlayColor: '#2e9dbd',
        overlayOpacity: 0.7,
        zoomH: 25,
        zoomW: 15,
        speed: 300
    };
     
    var settings = jQuery.extend(defaults, settings);
 
    return this.each(function() {
    
        var s = settings;
        var hz = jQuery(this);
        var image = jQuery('img', jQuery(this));

        var width = 220;
        var height = 102;
    
        hz.hover(function() {
            jQuery('img', this).stop().animate({
                height: 102 + s.zoomH,
                width: 220 + s.zoomW,
                marginLeft: -(s.zoomW/2),
                marginTop: -(s.zoomH/2)
            }, s.speed);
        }, function() {
            jQuery('img', this).stop().animate({
                    height: 102,
                    width: 220,
                    marginLeft: 0,
                    marginTop: 0
                }, s.speed);
            });
        });
    }
});