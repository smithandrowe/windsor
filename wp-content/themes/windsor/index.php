<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

		<div id="primary">
			<div id="content" role="main">
			<div class="featured-issue">
			<?php query_posts('cat=4&showposts=1'); ?>
			<?php if ( have_posts() ) : ?>

				<?php twentyeleven_content_nav( 'nav-above' ); ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', get_post_format() ); ?>

				<?php endwhile; ?>

				<?php //twentyeleven_content_nav( 'nav-below' ); ?>

			<?php else : ?>
			
				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Nothing Found', 'twentyeleven' ); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'twentyeleven' ); ?></p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->

			<?php endif; ?>
			</div>
			<div class="other-issues">
				<h2>Our other recent issues</h2>
			<?php query_posts('cat=5&showposts=1'); ?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" class="post no-results not-found">
					<div class="issue-thumb">
					<a href="<?php the_permalink(); ?>"><img src="
						<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							echo $feat_image;
						?>" title="<?php the_title(); ?>" height="100%" width="100%" />
						
					</a>
					<div class="caption">
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</a>
					</div>
				</article>
				<?php endwhile; ?>
			<?php endif; ?>
			<?php query_posts('cat=7&showposts=1'); ?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" class="post no-results not-found">
					<div class="issue-thumb">
					<a href="<?php the_permalink(); ?>"><img src="
						<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							echo $feat_image;
						?>" title="<?php the_title(); ?>" height="100%" width="100%" />
						
					</a>
					<div class="caption">
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</a>
					</div>
				</article>
				<?php endwhile; ?>
			<?php endif; ?>
			<?php query_posts('cat=8&showposts=1'); ?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" class="post no-results not-found">
					<div class="issue-thumb">
					<a href="<?php the_permalink(); ?>"><img src="
						<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							echo $feat_image;
						?>" title="<?php the_title(); ?>" height="100%" width="100%" />
						
					</a>
					<div class="caption">
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</a>
					</div>
				</article>
				<?php endwhile; ?>
			<?php endif; ?>
			<?php query_posts('cat=9&showposts=1'); ?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" class="post no-results not-found">
					<div class="issue-thumb">
					<a href="<?php the_permalink(); ?>"><img src="
						<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							echo $feat_image;
						?>" title="<?php the_title(); ?>" height="100%" width="100%" />
						
					</a>
					<div class="caption">
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</a>
					</div>
				</article>
				<?php endwhile; ?>
			<?php endif; ?>
			<?php query_posts('cat=10&showposts=1'); ?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" class="post no-results not-found">
					<div class="issue-thumb">
					<a href="<?php the_permalink(); ?>"><img src="
						<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							echo $feat_image;
						?>" title="<?php the_title(); ?>" height="100%" width="100%" />
						
					</a>
					<div class="caption">
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</a>
					</div>
				</article>
				<?php endwhile; ?>
			<?php endif; ?>
			<?php query_posts('cat=6&showposts=1'); ?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" class="post no-results not-found">
					<div class="issue-thumb">
						<a href="<?php the_permalink(); ?>"><img src="
							<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
								echo $feat_image;
							?>" title="<?php the_title(); ?>" height="100%" width="100%" />
							
						</a>
						<div class="caption">
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</a>
					</div>
				</article>
				<?php endwhile; ?>
			<?php endif; ?>
			</div>
			<div class="clear">&nbsp;</div>
			<div class="advertisment leaderboard">
				
			<?php 
				// Ads Place output
				if(function_exists('drawAdsPlace')) drawAdsPlace(array('id' => 1), true);
			?>
			</div>
			</div><!-- #content -->
		</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>