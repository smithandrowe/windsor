<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header();  ?>

		<div id="primary">
			<div id="content" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'page' ); ?>

					<?php comments_template( '', true ); ?>

				<?php endwhile; // end of the loop. ?>

			</div><!-- #content -->
		</div><!-- #primary -->
<script language="JavaScript" type="text/javascript">
jQuery(function(){
    var bgImages = [ '<?php echo get_bloginfo('template_url'); ?>/images/hero/hero_royale.jpg', '<?php echo get_bloginfo('template_url'); ?>/images/hero/hero_genesis.jpg', '<?php echo get_bloginfo('template_url'); ?>/images/hero/hero_rapid.jpg' ];
    var currImage = 1;
    setInterval( function(){
    	if (currImage > 2) {
    		currImage = 0;
    	}
    	jQuery('#bgs').fadeTo('slow', 0, function() {jQuery(this).css('background-image', 'url(' + bgImages[currImage] + ')');currImage = currImage + 1;}).fadeTo('slow', 1);
		
    }, 6000)
})

</script>
<div id="bgs">
	
</div><div class="grid_12">
<p>&nbsp;</p>
<em class="fineprint">Please note all photographs shown are 2012 models</em>
</div>
<?php get_footer(); ?>