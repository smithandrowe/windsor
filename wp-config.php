<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'windsor');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'LBx?XG!)~a~B]jPw[%i^ZB+{ID_@%2#^7_FsX-ejqs9/Qc-zx[Lr!iSpK=D6F>a!');
define('SECURE_AUTH_KEY',  '[efH#vh_Yj;}tod7xrQAvevpC6HSj[x1.@Cg,P>v)yD4j<=_N*(-}9.35%|v.wkQ');
define('LOGGED_IN_KEY',    'xK{*21r_(x]55CpT0O*;No#K5C>7cg/Bwx,7F}Yws53{w-qPn^/uC?gN.YF8t]eS');
define('NONCE_KEY',        'tOpgB`[,&6=my!Q|#{<9E_t#N)|wFb}4(Z,ow&reEI-0-#T4E>0lI?02%}DgE4rk');
define('AUTH_SALT',        '[=8Uk%#!T,3cus,r_Bw5(mN+ZJb3%#OjeitbX[UO~UImS,~vVQAQ%!&SjHUN)</-');
define('SECURE_AUTH_SALT', 'Mb:s8,Dj&?Dn+DrY0SDN-U*^#cg^#}$^N%^<(||!M:^0D:P(&0M-26e#4vyaT{dK');
define('LOGGED_IN_SALT',   '%1n/b2H9FFaS,uXSbN4)f^#FKMV*pnOW(N2XmP(7PohmV]v~mE;No$7*P5ouBvbR');
define('NONCE_SALT',       ')rJ.u5z[2GiIlbz_uIy/=b[G<Oa?+WN@_Uc 6&83mCVF$>je|Y{M5gt,:E`^P[P_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
