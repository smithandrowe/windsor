<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

		<section id="primary">
			<div id="content" role="main">
			<h1>Current Issue</h1>
			<div class="featured-issue">
			<?php query_posts('cat=' . get_query_var('cat') . '&showposts=1'); ?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h2 class="entry-title">
							<a rel="bookmark" title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</h2>
						<p><?php echo the_date( "M Y", "", "", true); ?></p>
					</header>
					<a href="<?php the_permalink(); ?>"><img src="
						<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							echo $feat_image;
						?>" title="<?php the_title(); ?>" width="100%" />
					</a>
					<div class="actions">
						<div class="current"><div class="bracket">&#125;</div><a href="<?php the_permalink(); ?>">READ ISSUE</a></div>
					</div>
				</article>
				<?php endwhile; ?>
			<?php else: ?>
				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Nothing Found', 'twentyeleven' ); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'twentyeleven' ); ?></p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->

			<?php endif; ?>
			</div>
			<?php query_posts('cat=' . get_query_var('cat') . '&offset=1'); ?>
			<div class="other-issues">
				<h2>Previous Issues</h2>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" class="post no-results not-found">
						<header class="entry-header">
							<div class="entry-meta">
								<?php echo the_date( "M Y", "", "", true); ?> 
							</div><!-- .entry-meta -->
						</header>
						<a href="<?php the_permalink(); ?>"><img src="
							<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
								echo $feat_image;
							?>" title="<?php the_title(); ?>" width="100%" />
						</a>
						<div class="actions">
							<div class="current"><div class="bracket">&#125;</div><a href="<?php the_permalink(); ?>">READ ISSUE</a></div>
						</div>
					</article>
					<?php endwhile; ?>
				<?php else: ?>
					<p>Sorry, no previous issues found.</p>
				<?php endif; ?>
			</div>
			<div class="clear">&nbsp;</div>

			</div><!-- #content -->
		</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
