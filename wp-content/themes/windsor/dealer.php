<?php
/*
Template Name: Dealer
*/
get_header(); ?>

		<div id="primary" class="dealer-page">
			<div id="content" role="main">
				<?php if (have_posts()) : while (have_posts()) : the_post();?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
	<div class="grid_6">
		<h1 class="entry-title"><?php the_title(); ?></h1>
	
	</div>
	<div class="grid_6">
		<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) );	?>" align="right" title="<?php the_title(); ?>" />
	</div>
	</header><!-- .entry-header -->
	<div class="entry-content">
		<?php the_content(); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->
	<footer class="entry-meta">
		<?php edit_post_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-meta -->
</article><!-- #post-<?php the_ID(); ?> -->
<?php endwhile; endif; ?>

			</div><!-- #content -->
		</div><!-- #primary -->

<?php get_footer(); ?>