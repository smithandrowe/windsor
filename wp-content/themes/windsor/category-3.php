<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

		<section id="primary">
			<div id="content" role="main">
			<div class="featured-issue">
			<?php query_posts('cat=4&showposts=1'); ?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<h1 class="entry-title">
							<a rel="bookmark" title="3000 Melbourne Magazine" href="<?php echo esc_url(get_category_link("4")); ?>">3000 Melbourne Magazine</a>
						</h1>
					</header>
					<a href="<?php the_permalink(); ?>"><img src="
						<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							echo $feat_image;
						?>" title="<?php the_title(); ?>" width="100%" />
					</a>
					<div class="actions">
						<div class="previous"><div class="bracket">&#123;</div><a href="<?php echo esc_url(get_category_link("4")); ?>">PREVIOUS ISSUES</a></div>
						<div class="current"><div class="bracket">&#125;</div><a href="<?php the_permalink(); ?>">CURRENT ISSUE</a></div>
					</div>
				</article>
				<?php endwhile; ?>
			<?php else: ?>
				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Nothing Found', 'twentyeleven' ); ?></h1>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'twentyeleven' ); ?></p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->

			<?php endif; ?>
			</div>
			<div class="other-issues">
				<h2 class="entry-title">Our other publications</h2>
				<hr class="visihr" />
			<?php query_posts('cat=5&showposts=1'); ?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" class="post no-results not-found">
					<header class="entry-header">
						<h2 class="entry-title">
							<a rel="bookmark" title="3008 Docklands Magazine" href="<?php echo esc_url(get_category_link("5")); ?>">3008 Docklands Magazine</a>
						</h2>
					</header>
					<a href="<?php the_permalink(); ?>"><img src="
						<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							echo $feat_image;
						?>" title="<?php the_title(); ?>" width="100%" />
					</a>
					<div class="actions">
						<div class="previous"><div class="bracket">&#123;</div><a href="<?php echo esc_url(get_category_link("5")); ?>">PREVIOUS ISSUES</a></div>
						<div class="current"><div class="bracket">&#125;</div><a href="<?php the_permalink(); ?>">CURRENT ISSUE</a></div>
					</div>
				</article>
				<?php endwhile; ?>
			<?php endif; ?>
			<hr />
			<?php query_posts('cat=7&showposts=1'); ?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" class="post no-results not-found">
					<header class="entry-header">
						<h2 class="entry-title">
							<a rel="bookmark" title="3207 Port Melbourne Magazine" href="<?php echo esc_url(get_category_link("7")); ?>">3207 Port Melbourne Magazine</a>
						</h2>
					</header>
					<a href="<?php the_permalink(); ?>"><img src="
						<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							echo $feat_image;
						?>" title="<?php the_title(); ?>" width="100%" />
					</a>
					<div class="actions">
						<div class="previous"><div class="bracket">&#123;</div><a href="<?php echo esc_url(get_category_link("7")); ?>">PREVIOUS ISSUES</a></div>
						<div class="current"><a href="<?php the_permalink(); ?>">CURRENT ISSUE<div class="bracket">&#125;</div></div>
					</div>
				</article>
				<?php endwhile; ?>
			<?php endif; ?>
			<hr />
			<?php query_posts('cat=8&showposts=1'); ?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" class="post no-results not-found last">
					<header class="entry-header">
						<h2 class="entry-title">
							<a rel="bookmark" title="3205 South Melbourne Magazine" href="<?php echo esc_url(get_category_link("8")); ?>">3205 South Melbourne Magazine</a>
						</h2>
					</header>
					<a href="<?php the_permalink(); ?>"><img src="
						<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							echo $feat_image;
						?>" title="<?php the_title(); ?>" width="100%" />
					</a>
					<div class="actions">
						<div class="previous"><div class="bracket">&#123;</div><a href="<?php echo esc_url(get_category_link("8")); ?>">PREVIOUS ISSUES</a></div>
						<div class="current"><a href="<?php the_title(); ?>">CURRENT ISSUE<div class="bracket">&#125;</div></div>
					</div>
				</article>
				<?php endwhile; ?>
			<?php endif; ?>
			<hr />
			<?php query_posts('cat=9&showposts=1'); ?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" class="post no-results not-found">
					<header class="entry-header">
						<h2 class="entry-title">
							<a rel="bookmark" title="Essendon Fields News" href="<?php echo esc_url(get_category_link("9")); ?>">Essendon Fields News</a>
						</h2>
					</header>
					<a href="<?php the_permalink(); ?>"><img src="
						<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							echo $feat_image;
						?>" title="<?php the_title(); ?>" width="100%" />
					</a>
					<div class="actions">
						<div class="previous"><div class="bracket">&#123;</div><a href="<?php echo esc_url(get_category_link("9")); ?>">PREVIOUS ISSUES</a></div>
						<div class="current"><a href="<?php the_permalink(); ?>">CURRENT ISSUE<div class="bracket">&#125;</div></div>
					</div>
				</article>
				<?php endwhile; ?>
			<?php endif; ?>
			<hr />
			<?php query_posts('cat=10&showposts=1'); ?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" class="post no-results not-found">
					<header class="entry-header">
						<h2 class="entry-title">
							<a rel="bookmark" title="3002 East Melbourne Jolimont News" href="<?php echo esc_url(get_category_link("10")); ?>">3002 East Melbourne Jolimont News</a>
						</h2>
					</header>
					<a href="<?php the_permalink(); ?>"><img src="
						<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							echo $feat_image;
						?>" title="<?php the_title(); ?>" width="100%" />
					</a>
					<div class="actions">
						<div class="previous"><div class="bracket">&#123;</div><a href="<?php echo esc_url(get_category_link("10")); ?>">PREVIOUS ISSUES</a></div>
						<div class="current"><a href="<?php the_permalink(); ?>">CURRENT ISSUE<div class="bracket">&#125;</div></div>
					</div>
				</article>
				<?php endwhile; ?>
			<?php endif; ?>
			<hr />
			<?php query_posts('cat=6&showposts=1'); ?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" class="post no-results not-found last">
					<header class="entry-header">
						<h2 class="entry-title">
							<a rel="bookmark" title="3004 News" href="<?php echo esc_url(get_category_link("6")); ?>">3004 News</a>
						</h2>
					</header>
					<a href="<?php the_permalink(); ?>"><img src="
						<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							echo $feat_image;
						?>" title="<?php the_title(); ?>" width="100%" />
					</a>
					<div class="actions">
						<div class="previous"><div class="bracket">&#123;</div><a href="<?php echo esc_url(get_category_link("6")); ?>">PREVIOUS ISSUES</a></div>
						<div class="current"><a href="<?php the_permalink(); ?>">CURRENT ISSUE<div class="bracket">&#125;</div></div>
					</div>
				</article>
				<?php endwhile; ?>
			<?php endif; ?>
			</div>
			<div class="clear">&nbsp;</div>
			</div>
			<!-- #content -->
		</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
